FROM python:3.6-alpine
EXPOSE 5000
RUN pip install flask

ENV APP_ROOT=/opt/talkie-toaster
ENV PATH=${APP_ROOT}/bin:${PATH} HOME=${APP_ROOT}
COPY webhook-listen ${APP_ROOT}/bin/
COPY uid_entrypoint ${APP_ROOT}/bin/
RUN chmod -R u+x ${APP_ROOT}/bin && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd

### Containers should NOT run as root as a good practice
USER 10001
VOLUME ${APP_ROOT}/etc/talkie-toaster
WORKDIR ${APP_ROOT}

### user name recognition at runtime w/ an arbitrary uid - for OpenShift deployments
ENTRYPOINT [ "uid_entrypoint" ]
CMD [ "webhook-listen" ]

