Talkie Toaster
==============

A dumb webhook in Python/flask to test usage of Gitlab hooks plus
deployment on Docker. It only requires Python 3 and Flask.

Local Run
---------
- Ensure you have `python` 3 and `flask` installed:

  ```console
  $ python --version
  Python 3.X.Y
  $ python -c "import flask"
  $
  ```
- If you don't have flask, it can be pip installed directly on
  in a virtualenv:

  ```console
  $ pip install flask
  ```
- Run the `webhook-listen` script:

  ```
  $ ./webhook-listen &
  ...
  ```
- This will start the hook on `http://0.0.0.0:5000` with a
  `POST` handler at `/webhook`. This can be pinged by doing:

  ```console
  $ curl -XPOST http://0.0.0.0:5000/webhook
  new post
  headers: Host: 0.0.0.0:5000
  Accept: */*
  User-Agent: curl/7.47.0


  data: b''
  end post
  127.0.0.1 - - [03/Aug/2018 15:34:06] "post /webhook HTTP/1.1" 200 -
  would you like some toast?
  $
  ```

  You can use `-d` arguments to `curl` as well.

Docker Run
----------
- Build an image using the `Dockerfile` in the root of the repo:

  ```console
  $ docker build -t talkie-toaster .
  ...
  ```
- To run and make the 5000 port available do:

  ```
  $ docker run --rm -p 5000:5000 -d -t talkie-toaster
  longcontainerid
  ```
- It can be pinged using curl in the same way:

  ```console
  $ curl -XPOST http://0.0.0.0:5000/webhook
  would you like some toast?
  $
  ```
- Full logs can be seen using:

  ```console
  $ docker logs longcontainerid

   * Serving Flask app "webhook-listen" (lazy loading)
   * Environment: production
     WARNING: Do not use the development server in a production environment.
     Use a production WSGI server instead.
   * Debug mode: on
   * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
   * Restarting with stat
   * Debugger is active!
   * Debugger PIN: 235-378-245
   new post
   headers: Host: 0.0.0.0:5000
   User-Agent: curl/7.47.0
  Accept: */*


  data: b''
  end post
  172.17.0.1 - - [03/Aug/2018 14:40:07] "post /webhook HTTP/1.1" 200 -
  $
  ```
- The container can be stopped and removed (because we supplied the `--rm`
  flag to `docker run`) with

  ```console
  $ docker stop longcontainerid
  ```

Prebuilt Docker Images
----------------------

For deployment, prebuilt images can be pulled from the Gitlab Docker Registry:

```console
$ docker pull gitlab-registry.cern.ch/bmorgan/talkie-toaster:tag

or from Docker Hub:

```console
$ docker run benmorgan/talkie-toaster
```

